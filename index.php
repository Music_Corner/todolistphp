<?php
require 'back/application/php/userAgentCheck.php';
require 'back/application/php/auth/authCheck.php';
require 'back/application/php/addBlock.php';
// подгружаем и активируем авто-загрузчик Twig-а
require_once 'vendor/autoload.php';
//Twig_Autoloader::register();
try {
  // указывае где хранятся шаблоны
  $loader = new Twig_Loader_Filesystem('/');

  // инициализируем Twig
  $twig = new Twig_Environment($loader);
    // подгружаем шаблон
  $template = $twig->loadTemplate('front/Pages/index.tmpl');
  // передаём в шаблон переменные и значения
  // выводим сформированное содержание
  echo $template->render(array(
      'auth' => $authStatus,
      'addBlock' => $block,
      'countNumber' => $maxCountNumberDiv,
      'userAgent' => $userAgent,
     ));
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}
?>
