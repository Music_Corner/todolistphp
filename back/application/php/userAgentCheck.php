<?php
function isMobile() {

    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

if(isMobile()){
    $userAgent = '<link rel="stylesheet" type="text/css" href="front/static/css/mobile.css">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">';
}
else {
    $userAgent = '<link rel="stylesheet" type="text/css" href="front/static/css/style.css">';
};
?>