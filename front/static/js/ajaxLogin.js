var pattern = /[^а-яА-Яa-zA-Z0-9_\-]/;
$(document).ready(function () {
    $('.errorWindow').hide();
    $(':input').on('input', handler);
            function handler() {
                // var key = event.key;
                console.log($(':input').val());
                    if (pattern.test($(':input').val())) {
                        $('.errorWindow').fadeIn();
                        $('#Login').fadeOut();
                        $('#Register').fadeOut();
                    } else {
                        $('.errorWindow').fadeOut();
                        $('#Login').fadeIn();
                        $('#Register').fadeIn();
                    }
            }
});

function sendLoginQuery() {
//Получаем параметры
    var login = $('input[name="login"]').val();
    var password = $('input[name="password"]').val();
        // Отсылаем паметры
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "back/application/php/ajax/ajaxLogin.php",
            data: {
                login: login,
                password: password,
            },
            //При удачном завершение запроса - выводим то, что нам вернул PHP
            success: function (data) {
                // console.log(data);
                if (data.status == true) {
                    alert('Logged in successfully');
                    document.location.href = 'index.php'
                } else {
                    alert('Log in is failed')
                }
            },
            error: function () {
                alert('Database is down, sorry');
            }
        });
}