(function() {
    $(document).ready(function () {
        let countNumber = $('div.hidden').attr('countnumber');
        init();

        function init() {
            let blocksCount = $('.fatherblock').each(function () {
                const i = $(this).attr('data-index');
                onClickEvents(i);
            });

           $('button#Add').click(function (event) {
                event.preventDefault();
                addBlock();
                send();
                $(this).prev().val('');
            });

            const logOutButton = $('#Logout');
            logOutButton.click(function () {
                document.location.href = "login.php";
            });
        }

        function onClickEvents(countNumber)  {
            console.log(countNumber);
            const deleteButton = $('[data-index="' + countNumber + '"] [name="Delete_Button"]');
            if (deleteButton) {
                deleteButton.click(function () {
                    delBlock(countNumber);
                });
            }

            const markButton = $('[data-index="' + countNumber + '"] [name="Mark_Button"]');

            if (markButton) {
                markButton.click(function () {
                    markText(countNumber);
                    sendMark(countNumber);
                });
            }

            const UnmarkButton = $('[data-index="' + countNumber + '"] [name="Unmark_Button"]');

            if (UnmarkButton) {
                UnmarkButton.click(function () {
                    unmarkText(countNumber);
                    sendUnmark(countNumber);
                });
            }

        }

        function addIndependentBlock(dataFromBlocks) {
            const {text, countNumber, mark} = dataFromBlocks;
            const newBlockHtml = generateBlockHTML(dataFromBlocks);
            const containerFromHtml = $('#container').append(newBlockHtml);

            onClickEvents(countNumber);
        }

        function addBlock() {
            countNumber++;
            const inputFromHtml = $('#enteredText').val();
            const dataFromBlocks = {
                text: inputFromHtml,
                countNumber: countNumber,
                markText: false
            };
            addIndependentBlock(dataFromBlocks);
        }

        function delBlock(countNumber) {
            const blockFromHtml = $('[data-index="' + countNumber + '"]');
            blockFromHtml.remove();
            sendDelete(countNumber);
        }


        function markText(countNumber) {
            const blockFromHtml = $('[data-index="' + countNumber + '"] [class="block"]');
            const markButton = $('[data-index="' + countNumber + '"] [name="Mark_Button"]');
            blockFromHtml.css("text-decoration", "line-through");
            markButton.text('Unmark');
            markButton.attr('name', 'Unmark_Button');
            markButton.click(function () {
                unmarkText(countNumber);
                sendUnmark(countNumber);
            });
        }

        function unmarkText(countNumber) {
            const blockFromHtml = $('[data-index="' + countNumber + '"] [class="block"]');
            const unmarkButton = $('[data-index="' + countNumber + '"] [name="Unmark_Button"]');
            blockFromHtml.css("text-decoration", "none");
            unmarkButton.text('Mark');
            unmarkButton.attr('name', 'Mark_Button');
            unmarkButton.click(function () {
                markText(countNumber);
                sendMark(countNumber);
            });
        }

        function generateBlockHTML(dataFromBlocks) {
            const {text, countNumber, mark} = dataFromBlocks;
            return  '<div class="fatherblock" data-index="' + countNumber + '">'
                            + '<div class="block">' + text + '</div>'
                            + '<button name="Delete_Button">Delete</button>'
                            + '<button name="Mark_Button">Mark</button>'
                        + '</div>'
                    +'</form>';
        }
    });
})();